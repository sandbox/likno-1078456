// $Id: awm_js.js,v 1.0.1 2010/08/10 16:54:13 askesson Exp $

/**
* @file likno_menus_js.js
* The JavaScript code necessary for the behavior of the admin page
*/

//toggle
Drupal.behaviors.togg = function (context) {

  // BEGIN colors and styling...
  $("#edit-reset").hover(
    function() {
	$("#awm-reset-tooltip").show();
    },
    function() {
	$("#awm-reset-tooltip").hide();
    });

  $(".awm_showAll").css('color', '#009900');
  $(".awm_showExcept").css('color', '#990000');

  $("div[id^=edit-AWM-items-from-menu-]").each(function() {
	$(this).css('padding-left', '100px');
  });

  $("div[id^=edit-AWM-pages-name-]").each(function() {
	$(this).css('padding-left', '100px');
	$(this).css('padding-right', '500px');
//	$(this).css('position', 'relative');
//	$(this).css('top', '-30px');
  });
  $("div[id^=edit-AWM-stories-name-]").each(function() {
	$(this).css('padding-left', '100px');
	$(this).css('padding-right', '500px');
  });
  $("div[id^=edit-AWM-taxonomies-name-]").each(function() {
	$(this).css('padding-left', '100px');
	$(this).css('padding-right', '500px');
  });

  $("div[id^=edit-AWM-taxonomies-newest-]").each(function() {
	$(this).css('float', 'left');
  });
  // END colors and styling...

  // BEGIN RESULTS IN "GENERATE STRUCTURE CODE"
  //hide the all of the element with class msg_body
  $(".awmToggleBody").hide();
  //toggle the componenet with class awmToggleBody   
  $(".awmToggleHead").click(function() {
      if ($(this).next(".awmToggleBody").css('display') == 'block') {
		$(".awmToggleBody").slideUp(600); 
      } else {
		$(".awmToggleBody").slideUp(600); 
		$(this).next(".awmToggleBody").slideToggle(600);
      }
  });
  //add css 
  $(".awmToggleHead").each(function() {
      $(this).css("color", "#027AC6");
      //$(this).css("text-decoration", "underline");
      $(this).css("cursor", "pointer");
  });
  // END RESULTS IN "GENERATE STRUCTURE CODE"


  // BEGIN SHOW HIDE OF MORE INFO PIC
  //onclick, show the div
  $("*[id^=awm_show_me]").each(function() {
	$(this).click(function() {
		$(this).next("*[id^=awm_show_this]").slideDown();
		return false;
	});
  });
  $("*[id^=awm_show_me_close]").each(function() {
	$(this).click(function() {
		$(this).parents("*[id^=awm_show_this]").slideUp();
		return false;
	});
  });
  // END SHOW HIDE OF MORE INFO PIC

  // BEGIN SHOW HIDE OF MORE INFO for admin/block (superfish)
  //onclick, show the div
  $("*[id^=sf_awm_show_me]").each(function() {
	$(this).click(function() {
		// IE? OPERA?	(SAFARI==CHROME??)
		if(!$.browser.safari)	$(this).parent().next("*[id^=sf_awm_show_this]").slideDown(); // parents - link located into for <label>
		else			$(this).next("*[id^=sf_awm_show_this]").slideDown();
		return false;
	});
  });
  $("*[id^=sf_awm_show_me_close]").each(function() {
	$(this).click(function() {
		$(this).parents("*[id^=sf_awm_show_this]").slideUp();
		return false;
	});
  });
  // END SHOW HIDE OF MORE INFO for admin/block (superfish)

  // BEGIN FORM ENABLING/DISABLING (existing or new menu)
  $(".awm_menu_structure_enabling").each(function() {
	$(this).change(function() {
		if ($(this).nextAll('.awm_indicate_txt').css('color') == 'green') {
			$(this).nextAll('.awm_indicate_txt').css('color', 'red').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will NOT appear in your site');
		} else {
			$(this).nextAll('.awm_indicate_txt').css('color', 'green').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will appear in your site');
		}
	});
  });


  $("*[name=AWM_new_menu_1]").change(function()
  {
	if ($("*[name=AWM_items_from_menu_1]").attr("disabled") || $("*[name=AWM_items_from_menu_0]").attr("disabled") == "true") {
		$("*[name=AWM_items_from_menu_1]").attr("disabled", false);
		$(".awm_menu_structure_1").attr("disabled", true);
	} else {
		$("*[name=AWM_items_from_menu_1]").attr("disabled", true);
		$(".awm_menu_structure_1").attr("disabled", false);
	}
  });
  $("*[name=AWM_new_menu_2]").change(function()
  {
	if ($("*[name=AWM_items_from_menu_2]").attr("disabled") || $("*[name=AWM_items_from_menu_0]").attr("disabled") == "true") {
		$("*[name=AWM_items_from_menu_2]").attr("disabled", false);
		$(".awm_menu_structure_2").attr("disabled", true);
	} else {
		$("*[name=AWM_items_from_menu_2]").attr("disabled", true);
		$(".awm_menu_structure_2").attr("disabled", false);
	}
  });
  $("*[name=AWM_new_menu_3]").change(function()
  {
	if ($("*[name=AWM_items_from_menu_3]").attr("disabled") || $("*[name=AWM_items_from_menu_0]").attr("disabled") == "true") {
		$("*[name=AWM_items_from_menu_3]").attr("disabled", false);
		$(".awm_menu_structure_3").attr("disabled", true);
	} else {
		$("*[name=AWM_items_from_menu_3]").attr("disabled", true);
		$(".awm_menu_structure_3").attr("disabled", false);
	}
  });
  $("*[name=AWM_new_menu_4]").change(function()
  {
	if ($("*[name=AWM_items_from_menu_4]").attr("disabled") || $("*[name=AWM_items_from_menu_0]").attr("disabled") == "true") {
		$("*[name=AWM_items_from_menu_4]").attr("disabled", false);
		$(".awm_menu_structure_4").attr("disabled", true);
	} else {
		$("*[name=AWM_items_from_menu_4]").attr("disabled", true);
		$(".awm_menu_structure_4").attr("disabled", false);
	}
  });
  $("*[name=AWM_new_menu_5]").change(function()
  {
	if ($("*[name=AWM_items_from_menu_5]").attr("disabled") || $("*[name=AWM_items_from_menu_5]").attr("disabled") == "true") {
		$("*[name=AWM_items_from_menu_5]").attr("disabled", false);
		$(".awm_menu_structure_5").attr("disabled", true);
	} else {
		$("*[name=AWM_items_from_menu_5]").attr("disabled", true);
		$(".awm_menu_structure_5").attr("disabled", false);
	}
  });
  // END FORM ENABLING/DISABLING (existing or new menu)


  // BEGIN FORM TYPE INFO
  $("input[id^=edit-AWM-menu-type]").each(function() {
	$(this).change(function() {
		//which of the menus are we editing?
		awm_tohide_id = $(this).attr('name').substr($(this).attr('name').length -1, $(this).attr('name').length);
		// hide all other infos of the same menu
		$("div[id^=AWM_menu_type_info_"+awm_tohide_id+"]").each(function() {
			$(this).css('display', 'none');
		});

		// IE? OPERA?	(SAFARI==CHROME??)
		if($.browser.safari)	awm_toshow = $(this).next();
		else			awm_toshow = $(this).parent().next();
		if (awm_toshow.css('display') == 'block') {
			awm_toshow.css('display', 'none')
		} else {
			awm_toshow.css('display', 'block')
		}
	});
  });
  // END FORM TYPE INFO

 };
