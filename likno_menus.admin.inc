<?php
// $Id: likno_menus.admin.inc,v 1.0.1 2010/08/10 17:35:12 askesson Exp $

/**
 * @file likno_menus.admin.inc
 * Functions that are only called on the admin pages.
 */

/* This function unzips a file
 * name: the name of the file to unzip
 * folder: the target location of the unzipped files
 */

function likno_menus_unzipper($name, $folder){

    $zip = new ZipArchive;
     $res = $zip->open($name);
     if ($res === TRUE) {
         $zip->extractTo($folder);
         $zip->close();
         return 1;
     } else {
         return 0;
     }
}
/* This function is a validator for drupal: file_save_upload
 * $file parameter is a drupal file class added by the validation.
 * $awm_t: is the the number of the menu we are uploding for
 */
function likno_menus_validate_filename($file, $awm_t) {
  $errors = array();

    $filename = variable_get('AWM_menu_name_'.$awm_t, 'menu'.$awm_t).".zip";
    if ($file->filename != $filename) {
      $errors[] = t('Filename must be : %filename.', array('%filename' => $filename));
    
  }
  return $errors;
}

/**
 * Module settings form.
 */
function likno_menus_settings(&$form_state) {
  global $base_url;
  $form = array('#attributes' => array('enctype' => 'multipart/form-data'));
  $form['space-1'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<br>',
  );
  // AWM online folder 
  $form['AWM_menu_folder'] = array(
    '#type' => 'textarea',
    '#title' => t('Online folder for menu files'),
    '#default_value' =>  variable_get('AWM_menu_folder', "/". drupal_get_path('module', 'likno_menus')."/menu/"),
    '#cols' => 10,
    '#rows' => 1,
    '#description' => t('Every time you compile your local Likno-Menus project you should upload the compiled menu files at this folder: 
<i>'. $base_url .'<span id="AWM_the_path">'. variable_get('AWM_menu_folder', "/". drupal_get_path('module', 'likno_menus')."/menu/") .'</span></i><br />(note: you need to create this online folder yourself)'),
  );
  $form['space0'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<br>',
  );
  $form['title0'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<b>Available Likno-Menus:</b>',
  );
$form['likno_menus_menu_selected'] = array('#type' => 'hidden','#attributes' => array('id'=>'likno_menus_menu_selected'));
//multiple menus
for ($awm_t=1; $awm_t<=variable_get('awm_total_tabs', 1); $awm_t++) {

  // AWM Main 
  $menu_name = variable_get('AWM_menu_name_'.$awm_t, 'menu'.$awm_t);
  $menu_name_source = (variable_get('AWM_menu_source_'.$awm_t, 'superfish') == 'superfish')? " (Superfish)" : " (AllWebMenus)";
  $menu_name_title = variable_get('AWM_menu_active_'.$awm_t, false)? $menu_name." - Enabled".$menu_name_source:$menu_name;
  if ($awm_t==1 && variable_get('AWM_menu_name_1', 'firsttime')=='firsttime') $menu_name_title = $menu_name." - Enabled".$menu_name_source; // first time loading fix so that the 1st menu is enabled
  $form['likno_menus_menu'.$awm_t] = array(
    '#type' => 'fieldset',
    '#title' => $menu_name_title,
    '#collapsible' => TRUE,
    //'#collapsed' => $awm_t==0? FALSE : TRUE,
    '#collapsed' => TRUE,
  );
  $awm_main_def = $awm_t==1? 1 : 0; // first time loading fix so that the 1st menu is enabled
  $awm_indic_color = variable_get('AWM_menu_active_'.$awm_t, false)? 'green' : 'red';
  if ($awm_t==1 && variable_get('AWM_menu_name_1', 'firsttime')=='firsttime') $awm_indic_color = 'green'; // first time loading fix so that the 1st menu is enabled
  $awm_indic_text = variable_get('AWM_menu_active_'.$awm_t, false)? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will appear in your site" : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will NOT appear in your site";
  if ($awm_t==1 && variable_get('AWM_menu_name_1', 'firsttime')=='firsttime') $awm_indic_text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This menu will appear in your site"; // first time loading fix so that the 1st menu is enabled
  $form['likno_menus_menu'.$awm_t]['AWM_menu_active_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('Show <i>'.variable_get('AWM_menu_name_'.$awm_t, 'menu'.$awm_t).
		  '</i> in site <span id="awm_indicate_txt'.$awm_t.'" class="awm_indicate_txt" style="color:'.$awm_indic_color.'">'.$awm_indic_text.'</span>'),
    '#default_value' =>  variable_get('AWM_menu_active_'.$awm_t, $awm_main_def), //by default show the 1st menu? (YES)
    '#attributes' => array('class'=>"awm_menu_structure_enabling"),
  );
  $form['likno_menus_menu'.$awm_t]['AWM_menu_name_'.$awm_t] = array(
    '#type' => 'textarea',
    '#title' => t('Menu name'),
    '#default_value' =>  variable_get('AWM_menu_name_'.$awm_t, 'menu'.$awm_t),
    '#cols' => 20,
    '#rows' => 1,
    '#description' => t('Please make sure that the "Menu name" value matches 
                        the value in the "Compiled Menu Name" property of the AllWebMenus project file 
                        (Tools > Project Properties > Folders). <a href="#" id="awm_show_me'.$awm_t.'">show me</a>'.
			'<div style="margin-top: 20px; display: none; background-color: #FEFCF5; border: #E6DB55 solid 1px;" id="awm_show_this'.$awm_t.'">'.
			'		<table>
						<tr><td style="width: 600px; text-align: center; padding-top: 10px; padding-bottom: 10px;"><strong>More info</strong> - <a href="#" id="awm_show_me_close'.$awm_t.'">close</a></td></tr>
						<tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
							<img src="'.$base_url.'/'.drupal_get_path('module', 'likno_menus').'/more_info.jpg" width="486" height="560" alt="More info" title="More info"/>
						</td></tr>
						<tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
							<a href="#" id="awm_show_me_close'.$awm_t.'">close</a>
						</td></tr>
					</table>
				</div>'),
  );


  $form['likno_menus_menu'.$awm_t]['upload'.$awm_t] = array(

      '#type' => 'file',

      '#title' => t('Attach new file'),

      '#size' => 40,
);

  $form['likno_menus_menu'.$awm_t]['upload_submit'] = array('#type' => 'submit','#attributes'=> array('onclick'=>'document.getElementById("edit-likno-menus-menu-selected").value='.$awm_t.';'), '#value' => t('Upload zip file containing source of all web menus'), '#submit' => array('likno_menus_system_settings_upload_submit') );

  $awm_existing_menus_all=menu_get_names();
  $awm_existing_menus=array();
  // REMOVE MENUS THAT DO NOT HANDLE NODES! (navigation menu)
  while ($item = current($awm_existing_menus_all)) {
    if ($item != 'navigation') $awm_existing_menus[] = array_shift($awm_existing_menus_all);
    else array_shift($awm_existing_menus_all);
  }

  $form['likno_menus_menu'.$awm_t]['AWM_new_menu_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('<strong>Get the items and structure from one of the existing Drupal menus:</strong>'),
    '#default_value' =>  variable_get('AWM_new_menu_'.$awm_t, 0),
    '#attributes' => array('class'=>"awm_menu_newold_".$awm_t),
  );
  $awm_disabled = FALSE;
  if (!variable_get('AWM_new_menu_'.$awm_t, 0))
	$awm_disabled = TRUE;
  if ($awm_existing_menus == NULL)
	$awm_existing_menus[] = "There are no menus currently defined in your Drupal site";
  $form['likno_menus_menu'.$awm_t]['AWM_items_from_menu_'.$awm_t] = array(
    '#type' => 'radios',
    '#options' => $awm_existing_menus,
    '#default_value' =>  variable_get('AWM_items_from_menu_'.$awm_t, 0),
    '#disabled' => $awm_disabled,
    '#attributes' => array('class'=>"awm_menu_newold_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['title1'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<strong>...OR create a new menu by setting the Menu Structure below:</strong>',
  );


  // Menu Structure:
  //home
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Structure'),
    '#collapsible' => TRUE,
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_include_home_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('"Home"'),
    '#default_value' =>  variable_get('AWM_include_home_'.$awm_t, 1),
    '#description' => t('A "Home" item that opens the site\'s Home Page.'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['space1'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<br>',
  );
  //pages
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_pages_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('Pages'),
    '#default_value' =>  variable_get('AWM_pages_'.$awm_t, 1),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_excluded_pages_'.$awm_t] = array(
    '#type' => 'textarea',
    '#title' => t('<span class="awm_showAll">Show all Pages</span> <span class="awm_showExcept">except the following</span>'),
    '#cols' => 20,
    '#rows' => 1,
    '#default_value' =>  variable_get('AWM_excluded_pages_'.$awm_t, ''),
    '#description' => t('Node IDs that correspond to pages, separated by commas (their sub-pages will also be excluded). Example: 34, 59, 140'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_pages_ms_'.$awm_t] = array(
    '#type' => 'radios',
    '#options' => array(
      'main' => t('Show Pages as Main Menu items'),
      'sub' => t('Group Pages under a Main Menu Item with name:'),
    ),
    '#default_value' =>  variable_get('AWM_pages_ms_'.$awm_t, 'main'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_pages_name_'.$awm_t] = array(
    '#type' => 'textarea',
    //'#title' => t('PagesName'),
    '#cols' => 10,
    '#rows' => 1,
    '#default_value' =>  variable_get('AWM_pages_name_'.$awm_t, 'Pages'),					
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );

  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['space2'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<br>',
  );
  //stories
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_stories_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('Stories'),
    '#default_value' =>  variable_get('AWM_stories_'.$awm_t, 0),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_excluded_stories_'.$awm_t] = array(
    '#type' => 'textarea',
    '#title' => t('<span class="awm_showAll">Show the following Stories</span>'),
    '#cols' => 20,
    '#rows' => 1,
    '#default_value' =>  variable_get('AWM_excluded_stories_'.$awm_t, ''),
    '#description' => t('Node IDs that correspond to stories, separated by commas. Example: 34, 59, 140'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_stories_ms_'.$awm_t] = array(
    '#type' => 'radios',
    '#options' => array(
      'main' => t('Show Stories as Main Menu items'),
      'sub' => t('Group Stories under a Main Menu Item with name:'),
    ),
    '#default_value' =>  variable_get('AWM_stories_ms_'.$awm_t, 'sub'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_stories_name_'.$awm_t] = array(
    '#type' => 'textarea',
    '#cols' => 10,
    '#rows' => 1,
    '#default_value' =>  variable_get('AWM_stories_name_'.$awm_t, 'Stories'),					
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );

  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['space3'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<br>',
  );
  //taxonomy
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_taxonomies_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('Taxonomies (Categories)'),
    '#default_value' =>  variable_get('AWM_taxonomies_'.$awm_t, 0),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_excluded_taxonomies_'.$awm_t] = array(
    '#type' => 'textarea',
    '#title' => t('<span class="awm_showAll">Show all Taxonomy terms</span> <span class="awm_showExcept">except the following</span>'),
    '#cols' => 20,
    '#rows' => 1,
    '#default_value' =>  variable_get('AWM_excluded_taxonomies_'.$awm_t, ''),
    '#description' => t('Node IDs that correspond to taxonomy terms, separated by commas (their sub-terms will also be excluded). Example: 34, 59, 140'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
/*  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_taxonomies_with_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('Only show Taxonomy Terms that contain posts'),
    '#default_value' =>  variable_get('AWM_taxonomies_with_'.$awm_t, 0),
    //'#description' => t('A "Home" item that opens the site\'s Home Page.'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
*/  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_vocab_taxonomies_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('Group Taxonomy terms by Vocabularies'),
    '#default_value' =>  variable_get('AWM_vocab_taxonomies_'.$awm_t, 0),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_taxonomies_ms_'.$awm_t] = array(
    '#type' => 'radios',
    '#options' => array(
      'main' => t('Show Taxonomies as Main Menu items'),
      'sub' => t('Group Taxonomies under a Main Menu Item with name:'),
    ),
    '#default_value' =>  variable_get('AWM_taxonomies_ms_'.$awm_t, 'sub'),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_taxonomies_name_'.$awm_t] = array(
    '#type' => 'textarea',
    '#cols' => 10,
    '#rows' => 1,
    '#default_value' =>  variable_get('AWM_taxonomies_name_'.$awm_t, 'Taxonomies'),					
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_taxonomies_newest_'.$awm_t] = array(
    '#type' => 'checkbox',
    '#title' => t('Also show the latest posts of each Category as its submenu items - number of items:&nbsp;&nbsp;(up to)&nbsp;&nbsp;'),
    '#default_value' =>  variable_get('AWM_taxonomies_newest_'.$awm_t, 0),
    '#disabled' => !$awm_disabled,
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['AWM_taxonomies_newest_num_'.$awm_t] = array(
    '#type' => 'textarea',
//    '#title' => t('Number of pages/stories'),
    '#cols' => 1,
    '#rows' => 1,
    '#description' => t('Value must be<br/>between 1 and 50.'),
    '#default_value' =>  variable_get('AWM_taxonomies_newest_num_'.$awm_t, 5),					
    '#attributes' => array('class'=>"awm_menu_structure_".$awm_t),
  );

  // Menu Type:
  $form['likno_menus_menu'.$awm_t]['menu_type'.$awm_t] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Type'),
    '#collapsible' => TRUE,
  );

  $form['likno_menus_menu'.$awm_t]['menu_type'.$awm_t]['AWM_menu_source_'.$awm_t] = array(
    '#type' => 'radios',
    '#title' => t('Please select whether you want your menu to be generated using the AllWebMenus software or the Superfish script'),
    '#options' => array(
      'allwebmenus' => t('Menu will be created by AllWebMenus.'),
      'superfish' => t('Menu will be created by Superfish. (Block-specific Superfish settings can be found at '. l('admin/build/block', 'admin/build/block') .') <a style="font-style:italic;" href="#" id="sf_awm_show_me'.$awm_t.'">more info</a>'.
			'<div style="margin-top: 20px; display: none; background-color: #FEFCF5; border: #E6DB55 solid 1px;" id="sf_awm_show_this'.$awm_t.'">'.
			'		<table>
						<tr><td style="width: 600px; text-align: center; padding-top: 10px; padding-bottom: 10px;"><strong>More info</strong> - <a href="#" id="sf_awm_show_me_close'.$awm_t.'">close</a></td></tr>
						<tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
							<p>For each menu generated by this module a new "block" is created. For menus created by the Superfish script this block must contain all the necessary information about the appearance and the behavior of the items and the menu. '.l('Here', 'admin/build/block').' you will find all the blocks of your site; in order to configure this specific menu\'s settings, you have to configure the block who\'s name is the name of this menu.</p>
						</td></tr>
						<tr><td style="width: 600px; text-align: center; padding-top: 0px; padding-bottom: 10px;">
							<a href="#" id="sf_awm_show_me_close'.$awm_t.'">close</a>
						</td></tr>
					</table>
				</div>'),
    ),
    '#default_value' =>  variable_get('AWM_menu_source_'.$awm_t, 'superfish'),
    '#attributes' => array('class'=>"awm-admin_block-tooltip"),
  );

  $awm_Dynamic_show = (variable_get('AWM_menu_type_'.$awm_t, 'Dynamic') == 'Dynamic')? 'block' : 'none';
  $awm_Mixed_show = (variable_get('AWM_menu_type_'.$awm_t, 'Dynamic') == 'Mixed')? 'block' : 'none';
  $awm_Static_show = (variable_get('AWM_menu_type_'.$awm_t, 'Dynamic') == 'Static')? 'block' : 'none';
  $form['likno_menus_menu'.$awm_t]['menu_type'.$awm_t]['AWM_menu_type_'.$awm_t] = array(
    '#type' => 'radios',
    '#title' => t('Please select how you want your menu to behave (only for menus created by AllWebMenus)'),
    '#options' => array(
      'Dynamic' => t('"Dynamic" Menu Type'.
			'<div style="float: right;  width: 500px; display:'.$awm_Dynamic_show.';" class="awm_itemInfo" id="AWM_menu_type_info_'.$awm_t.'_Dynamic">
				<p style="margin-top: 0px; padding-top: 0px;">You have selected to create a menu structure of "Dynamic Type".</p>
				<p>This means that the menu items in AllWebMenus will only be used for preview/styling purposes.</p>
				<p>In your actual blog these items will be ignored and the menu will be populated "dynamically" based on the plugin settings.</p>
				<p>The styles in AllWebMenus Style Editor will be used to form the actual menu items.</p>
			</div>'),
      'Mixed' => t('"Mixed" Menu Type'.
			'<div style="float: right;  width: 500px; display: '.$awm_Mixed_show.';" class="awm_itemInfo" id="AWM_menu_type_info_'.$awm_t.'_Mixed">
				<p style="margin-top: 0px; padding-top: 0px;">You have selected to create a menu structure of "Mixed Type".</p>
				<p>This means that your menu will contain both the items you create within AllWebMenus ("static") and the items you import from Drupal ("dynamic").</p>
				<p>The imported Drupal items will use the styles of the AllWebMenus Style Editor but their actual content will be populated "dynamically" based on the plugin settings.</p>
				<p>The static items you create within AllWebMenus will be shown as is.</p>
			</div>'),
      'Static' => t('"Static" Menu Type'.
			'<div style="float: right;  width: 500px; display: '.$awm_Static_show.';" class="awm_itemInfo" id="AWM_menu_type_info_'.$awm_t.'_Static">
				<p style="margin-top: 0px; padding-top: 0px;">You have selected to create a menu structure of "Static Type".</p>
				<p>Your menu will be edited (addition/removal/customization of items) within AllWebMenus only.</p>
				<p>Any changes on your online blog will not affect its items until you perform the "Save settings & Generate Menu Structure Code" action and <strong>re-import</strong> to AllWebMenus.</p>
				<p>This allows for maximum customization, as your online menu will show all items and styles customized within AllWebMenus.</p>
			</div>'),
    ),
    '#default_value' =>  variable_get('AWM_menu_type_'.$awm_t, 'Dynamic'),
    '#description' => t('<br/><p style="width: 140px; text-align: center;"> A <strong>Superfish</strong> menu\'s type is always "Dynamic".</p>'),
  );

 
}

  $form['awm_superfish_hid'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable hoverIntent detection. (<strong>only for menus created by Superfish</strong>)'),
    '#description' => t('<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.l('What is hoverIntent?', 'http://cherne.net/brian/resources/jquery.hoverIntent.html', array('attributes' => array('target' => '_blank'))).'</i>'),
    '#default_value' => variable_get('awm_superfish_hid', 1),
  );
  $form['likno_menus_menu'.$awm_t]['menu_structure'.$awm_t]['space3'.$awm_t] = array(
    '#type' => 'item',
    '#value' => '<i style="color:red;">Note!</i> Modify the appearance and behavior of a <strong>Superfish</strong> menu by configuring its Block settings at '. l('admin/build/block', 'admin/build/block') .'.</li></ul>',
  );
  $return = likno_menus_system_settings_form($form);

return $return;
}


function likno_menus_system_settings_form($form) {
  $form['buttons']['gener_submit'] = array('#type' => 'submit', '#value' => t('Generate menu structure code and save configuration'), '#submit' => array('likno_menus_system_settings_form_submit') );
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'), '#submit' => array('likno_menus_system_settings_form_submit') );
//  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults'), '#submit' => array('likno_menus_system_settings_form_submit') );
  $form['buttons']['tip'] = array(
    '#type' => 'item',
    '#value' => '<p style="padding: 5px; color:#494949; border: 1px solid #494949; background-color: #DDD;"><strong>This button will reset the values of <span style="color:red;">ALL</span> the menus</strong></p>',
    '#prefix' => '<div id="awm-reset-tooltip" style="display:none;float:right; padding-right:150px; position:relative; top:-100px; width:190px;">', 
    '#suffix' => '</div>', 
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }

  $form['#submit'][] = 'system_settings_form_submit';
  $form['#theme'] = 'system_settings_form';
  return $form;
}

function likno_menus_system_settings_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';

  // Exclude unnecessary elements.
  unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'], $form_state['values']['form_build_id']);

	$lkn_exclude = "-1";
	$lkn_include = -1;
	$awm_xml_message = "";
	$awm_xml_out = "";

  foreach ($form_state['values'] as $key => $value) {
	if (!strcmp(substr($key, 0, strlen($key)-1), "AWM_menu_active_") && $value==0) {
		$lkn_include++;
		$lkn_exclude = substr($key, -1);
	} else if (!strcmp(substr($key, 0, strlen($key)-1), "AWM_menu_active_")) {
		$lkn_include++;
	}

	
    if ($op == t('Reset to defaults')) {
      variable_del($key);
	  variable_set('AWM_menu_active_1', true); // first menu is by default enabled
    }
    else {
      if (is_array($value) && isset($form_state['values']['array_filter'])) {
        $value = array_keys(array_filter($value));
      }
      
      if ($key == 'AWM_menu_folder' ){
            if ($value[0]!= "/")
                $value = "/". $value;
            if ($value[strlen($value)-1]!= "/")
                $value .= "/";
        }
      variable_set($key, $value);
    }
  }
  
  if ($op == t('Reset to defaults')) {
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else if ($op == t('Generate menu structure code and save configuration')){
    $awm_xml_message = awm_create_xml();
    drupal_set_message(t($awm_xml_message.'<br>'));
    drupal_set_message(t('The configuration options have been saved.'));
  }
  else
  drupal_set_message(t('The configuration options have been saved.'));
  cache_clear_all();
  drupal_rebuild_theme_registry();
}


function likno_menus_system_settings_upload_submit($form, &$form_state) {
    global $base_url;
    global $base_path;

    $i = $form_state['values']['likno_menus_menu_selected'];
    $limits = array ( 'extensions' => "zip", 'i' => $i ) ;
    $validators = array('file_validate_extensions' => array($limits['extensions']),'likno_menus_validate_filename' => array($limits['i'] ) );
    $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';



    

    if ($_FILES['files']['size']['upload'.$i]){

        $file = file_save_upload('upload'.$i, $validators, false,FILE_EXISTS_REPLACE);

    }



    if ($file == false){
         drupal_set_message('Error uploading file ','error');
         drupal_rebuild_theme_registry();
         return;
    }

    $path= dirname($_SERVER['SCRIPT_FILENAME']).variable_get('AWM_menu_folder',"/". drupal_get_path('module', 'likno_menus')."/menu/");
    if (!file_check_directory($path)){

        if (!file_create_path($path)){
            drupal_set_message("Cannot find or create the path specified: ".$path,'error');
            drupal_rebuild_theme_registry();
            return ;
        }
    }
    //print_r($file);
        if (!likno_menus_unzipper($file->destination,$path)){
            drupal_set_message("Error unzipping file",'error');
            drupal_rebuild_theme_registry();
            return ;
        }
    

    drupal_set_message("Files has been uploaded succesfully.");
    drupal_rebuild_theme_registry();
}


/**
* Basic function that generates the XML code for the menu,
* along with all the necessary parts to display it to the user
*/
function awm_create_xml() {

	global $base_url;
	$awm_ic = 1000;

	$awm_xml_out = "Menu Structure Code for each menu:<ul><span>&nbsp;</span>";
	for ($awm_t=1; $awm_t<=variable_get('awm_total_tabs', 1); $awm_t++) {
		if (!variable_get('AWM_menu_active_'.$awm_t, '')) { // if menu is enabled 
			$awm_xml_out .= "<li class='awmToggleHeadNot'>".variable_get('AWM_menu_name_'.$awm_t, 'menu'.$awm_t)." - NOT enabled</li>";

		} else if (variable_get('AWM_menu_source_'.$awm_t, 'superfish') == 'superfish') {
			$awm_xml_out .= "<li class='awmToggleHeadNot'>".variable_get('AWM_menu_name_'.$awm_t, 'menu'.$awm_t)." - Superfish Menu (Block-specific menu settings can be found at ". l('admin/build/block', 'admin/build/block') .")</li>";

		} else {

			$awm_xml_out .= "<li class='awmToggleHead'><a>".
								 variable_get('AWM_menu_name_'.$awm_t, 'menu'.$awm_t)." - AllWebMenus Menu (Click here to get its Menu Structure Code)</a></li>".
								 "<div class='awmToggleBody'>".
								 "<textarea cols='89' rows='10' id='loginfo' name='loginfo'>";
			$awm_xml_out .= "<?xml version='1.0' encoding='UTF-8'?>\n<mainmenu>"; //xml start 

			// xml menu type
			if ((variable_get('AWM_menu_type_'.$awm_t, 'Static') == 'Dynamic')) $awm_xml_out .= "<menutype>Dynamic</menutype>"; 
			if ((variable_get('AWM_menu_type_'.$awm_t, 'Static') == 'Mixed')) $awm_xml_out .= "<menutype>Mixed</menutype>"; 
			if ((variable_get('AWM_menu_type_'.$awm_t, 'Static') == 'Static')) $awm_xml_out .= "<menutype>Static</menutype>"; 

			$awm_xml_out .= AWM_get_items($awm_t, $awm_parentgroup, $awm_ic, true);

			$awm_xml_out .= "</mainmenu>"; //xml end 
			$awm_xml_out .= "</textarea>";

			$awm_xml_out .= '<div style="padding-left: 80px; width: 600px; text-align: left; padding-top: 10px; padding-bottom: 10px;">'.
							'- Press <strong>Ctrl+C</strong> to copy the above code'.
							'<br>- Switch to the AllWebMenus desktop application'.
							'<br>- Open the <i>"Add-ins -> Drupal Menu -> Import/Update Menu Structure from Drupal"</i> form'.
							'<br>- Paste the above copied "Menu Structure Code"'.
							'<br>- Configure further your menu (styles, etc.) through the AllWebMenus properties'.
							'<br>- Compile your menu from "Add-ins -> Drupal Menu -> Compile Drupal Menu"'.
							'<br>- Make sure that you upload all compiled files in the <strong>'.$base_url.variable_get('AWM_menu_folder', "/". drupal_get_path('module', 'likno_menus')."/menu/").'</strong> directory on your server'.
							'</div>';
			$awm_xml_out .= "</div>";			
		}
	}
	$awm_xml_out .= "</ul>";
	return $awm_xml_out;
}



